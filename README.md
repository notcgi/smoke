Test task for smoke
=====================

Install
------------

```
git clone https://gitlab.com/2ravno2/smoke.git
cd smoke
```

Create .env file. Example:
```
APP_ENV=dev
APP_SECRET=7eeefbdef3863d3f5aa4655e114ae0ee

DATABASE_URL=mysql://user:pass@127.0.0.1:3306/smoke?serverVersion=5.7
```

```
composer i
php bin/console doctrine:migrations:migrate
symfony serve
```


Description
------------

/       -- Main page
/admin  -- admin