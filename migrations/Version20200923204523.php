<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200923204523 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE blog_post ADD created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE classification__category ADD media_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE classification__category ADD CONSTRAINT FK_43629B36EA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_43629B36EA9FDD75 ON classification__category (media_id)');
        $this->addSql('ALTER TABLE classification__collection ADD media_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE classification__collection ADD CONSTRAINT FK_A406B56AEA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_A406B56AEA9FDD75 ON classification__collection (media_id)');
        $this->addSql('CREATE UNIQUE INDEX tag_collection ON classification__collection (slug, context)');
        $this->addSql('CREATE UNIQUE INDEX tag_context ON classification__tag (slug, context)');
        $this->addSql('ALTER TABLE product ADD popular TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE blog_post DROP created');
        $this->addSql('ALTER TABLE classification__category DROP FOREIGN KEY FK_43629B36EA9FDD75');
        $this->addSql('DROP INDEX IDX_43629B36EA9FDD75 ON classification__category');
        $this->addSql('ALTER TABLE classification__category DROP media_id');
        $this->addSql('ALTER TABLE classification__collection DROP FOREIGN KEY FK_A406B56AEA9FDD75');
        $this->addSql('DROP INDEX IDX_A406B56AEA9FDD75 ON classification__collection');
        $this->addSql('DROP INDEX tag_collection ON classification__collection');
        $this->addSql('ALTER TABLE classification__collection DROP media_id');
        $this->addSql('DROP INDEX tag_context ON classification__tag');
        $this->addSql('ALTER TABLE product DROP popular');
    }
}
